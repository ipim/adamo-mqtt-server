
require('dotenv').config({ path: '../.env' })
var aedes = require('aedes')()
const port = process.env.MQTT_PORT || 4711;
const wsPort = process.env.MQTT_WS_PORT || 1883;
console.log('Do we have env variables?', process.env.MQTT_PORT,  process.env.MQTT_WS_PORT,)
console.log('What is set?', port, wsPort)
// let port = 1883
// let wsPort = 4711
const server = require('net').createServer(aedes.handle);
const httpServer = require('http').createServer()
const ws = require('websocket-stream')
ws.createServer({ server: httpServer }, aedes.handle)

server.listen(port, function() {
    console.log('Ades MQTT listening on port: ' + port)
})

httpServer.listen(wsPort, function () {
  console.log('Aedes MQTT-WS listening on port: ' + wsPort)
    aedes.publish({ topic: 'aedes/hello', payload: "I'm broker " + aedes.id })
});



// fired when a  client is connected
aedes.on('client', function (client) {
  console.log('client connected', client.id);
});

aedes.on('clientError', function (client) {
  console.log('client disconnect', client.id);
});
aedes.on("connectionError", function (client, err) {
  console.log("client error", client, err.message, err.stack);
  aedes.close();
});

aedes.on("clientDisconnect", function (client) {
  console.log("Client disconnected", client.id);
});

// fired when a message is received
aedes.on('publish', function (packet, client) {
  console.log('Published : ', packet);
});

// fired when a client subscribes to a topic
// aedes.on('subscribe', function (topic, client) {
aedes.on('subscribe', function (subscriptions, client) {
  console.log('subscribe : ', subscriptions, client.id);

  let lastindex = subscriptions.length - 1
  // console.log('subscribed : ', topic);
  var topic = subscriptions[lastindex].topic
  if (topic.startsWith('MODEL/model')) {
    aedes.publish({
      // topic syntax: 'MODEL/model_ID_VERSION'
      topic: 'mqtt/subscribed/' + topic.split('_')[1] + '/' + topic.split('_')[2],
      payload: new Buffer(JSON.stringify(client.id)),
      qos: 1 // this is important for offline messaging
    });
  }
});

// fired when a client subscribes to a topic
aedes.on('unsubscribe', function (subscriptions, client) {
  console.log('unsubscribe : ', subscriptions, client.id);
  
  let lastindex = subscriptions.length - 1
  // console.log('subscribed : ', topic);
  var topic = subscriptions[lastindex]
  if (topic.startsWith('MODEL/model')) {
    aedes.publish({
      //topic syntax: 'MODEL/model_ID_VERSION'
      topic: 'mqtt/unsubscribed/' + topic.split('_')[1] + '/' + topic.split('_')[2],
      payload: new Buffer(JSON.stringify(client.id)),
      qos: 1 // this is important for offline messaging
    });
  }
});

// fired when a client is disconnecting
aedes.on('clientDisconnecting', function (client) {
  console.log('clientDisconnecting : ', client.id);
});

// fired when a client is disconnected
aedes.on('clientDisconnected', function (client) {
  console.log('clientDisconnected : ', client.id);
});